import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';
import { AlertController, MenuController, Events } from '@ionic/angular';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private authService:AuthenticationService,private router: Router, private alertCtrl:AlertController,private menu:MenuController,private events:Events ) { }

  ngOnInit() {
  }

  async invalidLoginAlert(){
    const alert = await this.alertCtrl.create({
      header: 'Inicio de Sesion no Valido',
      message: 'Nombre de Usuario o Contraseña Incorrecto',
      buttons: ['OK']
    });
    return await alert.present();
  }


  loginUser(user:string,pass:string){
    this.authService.login(user,pass).then(succes=>{
      if(succes){
        this.createUser(this.authService.currentUser);
        if(this.authService.isEmpleador()){
          this.router.navigate(['principalEmpleador']);
        }else{
          this.router.navigate(['principalTrabajador']);
        }
      }
    }).catch(err => {
      this.invalidLoginAlert();
    });
  }
  
  createUser(user) {
    this.events.publish('user:created', user, Date.now());
  }

}

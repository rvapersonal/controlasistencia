import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JustificacionPage } from './justificacion.page';

describe('JustificacionPage', () => {
  let component: JustificacionPage;
  let fixture: ComponentFixture<JustificacionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JustificacionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JustificacionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { VacacionesPesosPage } from './vacaciones-pesos.page';

const routes: Routes = [
  {
    path: '',
    component: VacacionesPesosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [VacacionesPesosPage]
})
export class VacacionesPesosPageModule {}

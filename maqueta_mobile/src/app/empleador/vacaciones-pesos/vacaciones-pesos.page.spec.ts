import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VacacionesPesosPage } from './vacaciones-pesos.page';

describe('VacacionesPesosPage', () => {
  let component: VacacionesPesosPage;
  let fixture: ComponentFixture<VacacionesPesosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VacacionesPesosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VacacionesPesosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
